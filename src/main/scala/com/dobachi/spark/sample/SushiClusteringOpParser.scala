package com.dobachi.spark.sample

case class SushiClustringConfig(inputPath: String = "",
                                outputPath: String ="",
                                numClusters: Int = 3,
                                iterNum: Int = 10)
/**
 * Option parser
 */
@SerialVersionUID(1L)
class SushiClustringOpParser() extends Serializable{
  val parser = new scopt.OptionParser[SushiClustringConfig]("SushiRecommend") {
    arg[String]("inputPath") required() action {
      (x, c) => c.copy(inputPath = x)
    } text("Path of input data")

    arg[String]("outputPath") required() action {
      (x, c) => c.copy(outputPath = x)
    } text("Path of output data")

    arg[Int]("numClusters") required() action {
      (x, c) => c.copy(numClusters = x)
    } text("Number of clusters")

    opt[Int]('i', "iterNum") valueName("<int number>") action {
      (x, c) => c.copy(iterNum = x)
    } text("Number of iteration")
   }

  def getParser() = {
    parser
  }
}

object SushiClustringOpParser {
  val sushiClusteringOpParser = new SushiClustringOpParser()

  def apply() = {
    sushiClusteringOpParser
  }
}

