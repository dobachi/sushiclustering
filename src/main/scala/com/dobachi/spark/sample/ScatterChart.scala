package com.dobachi.spark.sample

import java.io.{IOException, File}

import org.jfree.chart._
import org.jfree.data.general.DatasetUtilities
import org.jfree.data.xy.XYSeriesCollection
import org.jfree.chart.plot.PlotOrientation

import scalax.chart.api._

/**
 * Tool to generate graphs.
 */
object ScatterChart {

  def createView(pointGroup: Array[Array[Array[Double]]]): Unit = {
    val chart = generateChart(pointGroup)
    generateViewFromChart(chart)
  }

  def createView(pointGroup: Array[(Int, Array[Double])]): Unit = {
    val chart = generateChart(pointGroup)
    generateViewFromChart(chart)
  }

  /**
   * Create PNG file of graph
   *
   * @param pointGroup An array of points which have positive label
   * @param outputPath The output graph's path
   * @return
   */
  def createPng(pointGroup: Array[Array[Array[Double]]],
                outputPath: String): Unit = {
    val chart = generateChart(pointGroup)
    generatePngFromChart(chart, outputPath)
  }

  /**
   * Create PNG file of graph
   *
   * @param pointGroup An array of points which have positive label
   * @param outputPath The output graph's path
   * @return
   */
  def createPng(pointGroup: Array[(Int, Array[Double])],
                outputPath: String): Unit = {
    val chart = generateChart(pointGroup)
    generatePngFromChart(chart, outputPath)
  }


  /**
   * Generate Chart object
   * This is private method for generatePng method.
   *
   * @param pointGroup An array of points for each category
   */
  private def generateChart(pointGroup: Array[Array[Array[Double]]]): JFreeChart = {

    val data = new XYSeriesCollection()

    for((points, index) <- pointGroup.zipWithIndex) {
      val series = new XYSeries("Category" + index)
      points.map { p =>
        series.add(p(0), p(1))
      }

      data.addSeries(series)
    }

    ChartFactory.createScatterPlot(
      "Scatter Chart",
      "X",
      "Y",
      data,
      PlotOrientation.VERTICAL,
      true, false, false)
  }

  /**
   * Generate Chart object
   * This is private method for generatePng method.
   *
   * @param pointGroup An array of points for each category
   */
  private def generateChart(pointGroup: Array[(Int, Array[Double])]): JFreeChart = {

    val data = new XYSeriesCollection()

    val seriesMap = pointGroup.foldLeft(Map.empty[Int, XYSeries]){ (seriesMap, points) =>
      println(points._1.toString + " " + points._2.mkString(","))
      val series: XYSeries = seriesMap.getOrElse(points._1, new XYSeries("Category" + points._1))
      series.add(points._2(0), points._2(1))
      seriesMap.updated(points._1, series)
    }

    seriesMap.values.map(println)
    seriesMap.values.map(p => data.addSeries(p))

    ChartFactory.createScatterPlot(
      "Scatter Chart",
      "X",
      "Y",
      data,
      PlotOrientation.VERTICAL,
      true, false, false)
  }

  /**
   * Generate PNG Scatter graph
   *
   * @param chart JFreeChart object
   * @param outputPath The output path
   */
  private def generatePngFromChart(chart: JFreeChart, outputPath: String): Unit = {
    val outputFile = new File(outputPath)
    try {
      ChartUtilities.saveChartAsPNG(outputFile, chart, 1000, 1000)
    } catch {
      case e: IOException => e.printStackTrace()
    }
  }

  private def generateViewFromChart(chart: JFreeChart): Unit = {
    val frame = new ChartFrame("Simple Scatter Chart", chart)
    frame.pack()
    frame.setVisible(true)
  }
}

