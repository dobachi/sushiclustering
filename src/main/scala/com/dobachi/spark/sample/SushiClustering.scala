package com.dobachi.spark.sample

import org.apache.spark.{SparkContext, SparkConf, Logging}
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.clustering.KMeans
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.linalg.Matrix
import org.apache.spark.mllib.linalg.distributed.RowMatrix
/**
 * Sushi Recommend
 */
object SushiClustering {
  def main(args: Array[String]): Unit = {

    // Option parser
    val sushiClusteringOpParser = SushiClustringOpParser()

    sushiClusteringOpParser.getParser().parse(args, SushiClustringConfig()) exists { config =>

      val sparkConf = new SparkConf()
      val sc = new SparkContext(sparkConf)

      val sushiRecommend = new SushiClustering(sc, config.inputPath, config.outputPath, config.numClusters, config.iterNum)

      sc.stop()
      true
    }
  }
}

class SushiClustering(sc: SparkContext, inputPath: String, outputPath: String, numClusters: Int, iterNum: Int)
                                                            extends Serializable with Logging {

  val ifile = sc.textFile(inputPath)

  // create input data from input file
  val scores = ifile.map( p => p.split(" ") )
  val scoreVectors = scores.map( p=> Vectors.dense(p.map(_.toDouble)) )

  // execute clustering
  val clusters = KMeans.train(scoreVectors, numClusters, iterNum)

  // Obtain predicted clusters of each user
  val indices = clusters.predict(scoreVectors)

  // Convert format of data to use PCA
  val mat = new RowMatrix(scoreVectors)
  val pc = mat.computePrincipalComponents(2)
  val projected = mat.multiply(pc)

  // Create data to be visualized
  val pointAndIndices = indices.zip(projected.rows.map(_.toArray)).collect
  ScatterChart.createPng(pointAndIndices, outputPath)

}