###################
Sushi Recommend
###################
This is sample applicaiton of Spark MLlib.

*******************
Assumption
*******************
* You have installed sbt command

  + see: http://www.scala-sbt.org/

* You have HDSF/YARN cluster to execute application on YARN cluster.

**********
Prepare
**********

Input data
==========
Please Download input data from `Dr. Kamishima's web page <http://www.kamishima.net/sushi/>`_

Get data archives::

 $ cd
 $ mkdir Downloads
 $ cd ~/Downloads
 $ wget http://www.kamishima.net/asset/sushi3.tgz
 $ tar xvzf sushi3.tgz

Put data into HDFS::

 $ hdfs dfs -put sushi3/sushi3b.5000.10.score

*******************
How to make jar
*******************
Clone this repository::

 $ cd
 $ mkdir Sources
 $ cd Sources
 $ git clone https://bitbucket.org/dobachi/sushiclustering.git SushiClustering

Execute sbt::

 $ cd SushiClustering
 $ sbt assembly

Now, you have JAR file the following path::

 ${HOME}/Sources/SushiClustering/target/scala-2.10/sushiclustering.jar

**************************************************
How to submit application to YARN cluster
**************************************************
You can submit application with the following command::

 $ spark-submit --master yarn-client --class com.dobachi.spark.sample.SushiClustering ${HOME}/Sources/SushiClustering/target/scala-2.10/sushiclustering.jar hdfs://mycluster/user/vagrant/sushi3b.5000.10.score /tmp/sushiClustering.png 5

.. vim: ft=rst tw=0